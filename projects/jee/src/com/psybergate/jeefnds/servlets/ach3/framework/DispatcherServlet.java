package com.psybergate.jeefnds.servlets.ach3.framework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Properties;

@WebServlet(name = "Dispatcher", urlPatterns = "/*")
public class DispatcherServlet extends HttpServlet {

  private static Properties properties = null;

  public DispatcherServlet() throws IOException {
    super();
    try {
      InputStream inputStream = new FileInputStream("..\\webapps\\dispatcher\\WEB-INF\\classes\\com\\psybergate\\jeefnds\\servlets\\ach3\\config.properties");
      properties = new Properties();
      properties.load(inputStream);
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    }

  }

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathinfo = req.getPathInfo();
    try {
      String parameter = pathinfo.split("/")[1];

      if (parameter.equals("helloworld")) {
        Class GreetingController = Class.forName(properties.getProperty("greeting"));
        Method helloworld = GreetingController.getMethod("helloWorld", HttpServletRequest.class, HttpServletResponse.class);
        helloworld.invoke(GreetingController.newInstance(), req, resp);

      } else if (parameter.equals("goodbye")) {
        Class GreetingController = Class.forName(properties.getProperty("greeting"));
        Method goodbyeWorld = GreetingController.getMethod("goodbyeWorld", HttpServletRequest.class, HttpServletResponse.class);
        goodbyeWorld.invoke(GreetingController.newInstance(), req, resp);
      } else if (parameter.equals("getcurrentdate")) {
        Class DateController = Class.forName(properties.getProperty("date"));
        Method getCurrentDate = DateController.getMethod("getCurrentDate", HttpServletResponse.class);
        getCurrentDate.invoke(DateController.newInstance(), resp);
      } else if (parameter.equals("gettomorrowsdate")) {
        Class DateController = Class.forName(properties.getProperty("date"));
        Method getTomorrowsDate = DateController.getMethod("getTomorrowsDate", HttpServletResponse.class);
        getTomorrowsDate.invoke(DateController.newInstance(), resp);
      } else {
        throw new ServletException("Improper parameter");
      }
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw new ServletException("No paramaters provided");
    } catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException reflectionException) {
      reflectionException.printStackTrace();
      throw new ServletException("Server error");
    }
  }

  private Properties getProperties(String property) {
    return null;
  }
}
