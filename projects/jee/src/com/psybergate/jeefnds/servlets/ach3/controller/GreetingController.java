package com.psybergate.jeefnds.servlets.ach3.controller;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class GreetingController extends HttpServlet {

  public static void helloWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String name = req.getParameter("name");
    PrintWriter printWriter = resp.getWriter();
    printWriter.println("Hello " + name);
  }
  public static void goodbyeWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String name = req.getParameter("name");
    PrintWriter printWriter = resp.getWriter();
    printWriter.println("Goodbye " + name);
  }
}
