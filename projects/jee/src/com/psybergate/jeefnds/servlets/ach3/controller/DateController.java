package com.psybergate.jeefnds.servlets.ach3.controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

public class DateController {
  public static void getCurrentDate(HttpServletResponse resp) throws IOException {
    PrintWriter printWriter = resp.getWriter();
    printWriter.println(Calendar.getInstance().getTime());
  }

  public static void getTomorrowsDate(HttpServletResponse resp) throws IOException {
    Calendar calendar = Calendar.getInstance(); // Get calendar instance
    calendar.add(Calendar.DAY_OF_YEAR, 1); // add a day
    PrintWriter printWriter = resp.getWriter();
    printWriter.println(calendar.getTime()); // print the result
  }
}
