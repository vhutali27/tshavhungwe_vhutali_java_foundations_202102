package com.psybergate.jeefnds.servlets.ach3.Servlets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldServlet extends HttpServlet {
  public HelloWorldServlet() {
    super();
  }


  @Override
  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    PrintWriter printWriter = resp.getWriter();
    printWriter.println("Hello " + req.getParameter("name"));
    printWriter.println("PathInfo: " + req.getPathInfo());
    printWriter.println("PathTranslated: " + req.getPathTranslated());
  }
}
