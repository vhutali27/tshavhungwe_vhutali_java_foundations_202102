package com.psybergate.jeefnds.servlets.ach2.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class HelloWorldController {

  public void helloWorld(HttpServletRequest req, HttpServletResponse resp) throws IOException {
    String name = req.getParameter("name");
    PrintWriter printWriter = resp.getWriter();
    printWriter.println("Hello " + name);
  }
}
