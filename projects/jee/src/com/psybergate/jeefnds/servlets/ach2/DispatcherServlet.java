package com.psybergate.jeefnds.servlets.ach2;

import com.psybergate.jeefnds.servlets.ach2.controller.GetCurrentDateController;
import com.psybergate.jeefnds.servlets.ach2.controller.HelloWorldController;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@WebServlet(name = "Dispatch", urlPatterns = "/*")
public class DispatcherServlet extends HttpServlet {
  private static List<HelloWorldController> helloWorldControllers = new ArrayList<>();
  private static List<GetCurrentDateController> getCurrentDateControllers = new ArrayList<>();

  @Override
  protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
    String pathinfo = req.getPathInfo();
    try {
      String parameter = pathinfo.split("/")[1];

      if (parameter.equals("helloworld")) {
        HelloWorldController helloWorldController = new HelloWorldController();
        helloWorldController.helloWorld(req, resp);
        helloWorldControllers.add(helloWorldController);
      } else if (parameter.equals("getcurrentdate")) {
        GetCurrentDateController getCurrentDateController = new GetCurrentDateController();
        getCurrentDateController.getCurrentDate(resp);
        getCurrentDateControllers.add(getCurrentDateController);
      } else {
        throw new ServletException("Improper parameter");
      }
    } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
      throw new ServletException("No parameters provided");
    }
  }

}
