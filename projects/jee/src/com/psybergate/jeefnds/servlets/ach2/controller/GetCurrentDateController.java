package com.psybergate.jeefnds.servlets.ach2.controller;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;

public class GetCurrentDateController {
  public void getCurrentDate(HttpServletResponse resp) throws IOException {
    PrintWriter printWriter = resp.getWriter();
    printWriter.println(Calendar.getInstance().getTime());
  }
}
