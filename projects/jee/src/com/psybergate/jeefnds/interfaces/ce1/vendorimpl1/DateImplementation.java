package com.psybergate.jeefnds.interfaces.ce1;

public final class DateImplementation {

  public int year;
  public int month;
  public int day;

  public DateImplementation(int year, int month, int day) {
    this.year = year;
    this.month = month;
    this.day = day;
  }

  public DateImplementation(String date) throws InvalidDateException {
    String[] dates = date.split("-");
    this.year = Integer.parseInt(dates[0]);
    this.month = Integer.parseInt(dates[1]);
    this.day = Integer.parseInt(dates[2]);
    validDate(this);
  }

  private static boolean validDate(DateImplementation date) throws InvalidDateException {

    int month = date.month;
    if (month >= 1 && month <= 12) {
      int day = date.day;
      if (day >= 1 && day <= 31) {
        if (month == 2) {
          if (date.isLeapYear()) {
            if (day >= 29) {
              return false;
            }
          }
          if (day >= 28) {
            return false;
          }
        } else if (month < 8 && month % 2 == 0) {
          if (day == 31) {
            return false;
          }
        } else if (month > 7 && month % 2 == 0) {
          if (day != 31) {
            return false; // TODO: 2021/04/29 you stopped here
          }
        }

      } else {
        throw new InvalidDateException();
      }
    }
    return true;
  }

  private boolean isLeapYear() {
    if (year % 4 == 0 || year % 400 == 0) {
      return true;
    }
    return false;
  }

  private DateImplementation addDays(int days) throws InvalidDateException {
    try {
      return new DateImplementation(this.year, this.month, this.day + days);
    } catch (Exception e) {
      // TODO: 2021/04/30 implement 
    }
    return this; // TODO: 2021/04/30 implement 
  }
}
