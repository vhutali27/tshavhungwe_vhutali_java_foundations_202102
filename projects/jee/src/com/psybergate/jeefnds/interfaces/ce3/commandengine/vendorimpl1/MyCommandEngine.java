package com.psybergate.jeefnds.interfaces.ce3.commandengine.vendorimpl1;

import com.psybergate.jeefnds.interfaces.ce3.commandengine.standards.Command;
import com.psybergate.jeefnds.interfaces.ce3.commandengine.standards.CommandRequest;
import com.psybergate.jeefnds.interfaces.ce3.commandengine.standards.CommandResponse;

public class CommandEngine implements Command, CommandRequest, CommandResponse, com.psybergate.jeefnds.interfaces.ce3.commandengine.standards.CommandEngine {

  @Override
  public String getCommand() {
    return null;
  }

  @Override
  public void run() {

  }

  @Override
  public String request() {
    return null;
  }

  @Override
  public String getResponse() {
    return null;
  }
}
