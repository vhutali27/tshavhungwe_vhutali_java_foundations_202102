package com.psybergate.grad2021.annotations.hw2;

import com.psybergate.grad2021.annotations.hw1.MyAnnotations;

@MyAnnotations.DomainClass
public class Customer {

  @MyAnnotations.DomainProperty
  private String customerNum;

  @MyAnnotations.DomainProperty
  private String name;

  @MyAnnotations.DomainProperty
  private String surname;

  @MyAnnotations.DomainProperty
  private int dateOfBirth;

  @MyAnnotations.DomainTransient
  private int age;

}
