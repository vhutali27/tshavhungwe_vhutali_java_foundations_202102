package com.psybergate.grad2021.annotations.hw3;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.annotation.*;
import java.lang.reflect.Field;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@interface DomainClass {
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface DomainProperty {
  String type() default "VARCHAR(25)";

  String check() default "";

  boolean notNull() default false;

  boolean unique() default false;

  boolean primaryKey() default false;

  boolean foreignKey() default false;
}

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@interface DomainTransient {
}


public class DB2 {

  private static void writeToFile(String query) {
    try {
      File file = new File("Queries.sql");
      FileWriter fileWriter;
      if (file.createNewFile()) {
        System.out.println("Writing to recently created file: " + file.getName());
        fileWriter = new FileWriter(file);
      }
      else {
        System.out.println("Overwriting already existing file: " + file.getName());
        fileWriter = new FileWriter(file, false);
      }
      fileWriter.write(query);
      fileWriter.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    Class objectClass = Customer.class;
    Annotation[] annotations = objectClass.getAnnotations();

    String sqlQuery = "";
    for (Annotation annotation : annotations) {
      if (annotation instanceof DomainClass) {
        sqlQuery += "CREATE TABLE if not exists " + objectClass.getSimpleName() + "(";
        Field[] fields = objectClass.getDeclaredFields();
        for (Field field : fields) {
          annotations = field.getAnnotations();
          String fieldQuery = field.getName();
          String type = "VARCHAR(25)";
          String constraints = "";
          for (Annotation annotation1 : annotations) {
            if (!(annotation1 instanceof DomainTransient)) {
              if ((annotation1 instanceof DomainProperty)) {
                if (!((DomainProperty) annotation1).type().equals(" VARCHAR(25)")){
                  type = ((DomainProperty) annotation1).type();
                }
                constraints = "";
                if (!(((DomainProperty) annotation1).check().equals(""))) {
                  constraints += ((DomainProperty) annotation1).check();
                }
                if (((DomainProperty) annotation1).notNull() == true) {
                  constraints += " NOT NULL";
                }
                if (((DomainProperty) annotation1).unique() == true) {
                  constraints += " UNIQUE";
                }
                if (((DomainProperty) annotation1).primaryKey() == true) {
                  constraints += " PRIMARY KEY";
                }
                if (((DomainProperty) annotation1).foreignKey() == true) {
                  constraints += " FOREIGN KEY";
                }
                if ((constraints.equals(""))) {
                  continue;
                }
              }
            } else {
              continue;
            }
          }
          if (!sqlQuery.equals("CREATE TABLE if not exists " + objectClass.getSimpleName() + "(")) {
            sqlQuery += ",";
          }
          sqlQuery += " " + fieldQuery + " " + type + constraints;
        }
        sqlQuery += ");";
      }

      writeToFile(sqlQuery);
    }

    System.out.println("sqlQuery = " + sqlQuery);
  }
}


