package com.psybergate.grad2021.annotations.hw3;

import java.util.Random;

public class CustomerService {

  private static String getRandomMessage() {
    Random random = new Random();
    int length = random.nextInt(15);
    String string = "";
    for (int i = 0; i < length; i++) {
      string += (char) (97 + random.nextInt(26));
    }
    return string;
  }

  public static Customer randomCustomer() {

    Random random = new Random();

    return new Customer(getRandomMessage(), getRandomMessage(), 1950 + random.nextInt(50));
  }
}
