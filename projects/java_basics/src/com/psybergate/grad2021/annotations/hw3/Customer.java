package com.psybergate.grad2021.annotations.hw3;

@DomainClass
public class Customer {
  @DomainProperty(primaryKey = true)
  private String customerNum;

  private String name;

  private String surname;

  @DomainProperty(type = "DATE")
  private int dateOfBirth;

  @DomainTransient
  private int age;

  public Customer(String name, String surname, int dateOfBirth) {
    this.name = name;
    this.surname = surname;
    this.dateOfBirth = dateOfBirth;
  }
}
