package com.psybergate.grad2021.annotations.hw3;

// Doesn't work

import com.psybergate.grad2021.annotations.hw1.MyAnnotations;
import com.psybergate.grad2021.annotations.hw2.Customer;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

public class DatabaseManager {

  public static void generateDatabase(Object object) {
    Class<?> objectClass = object.getClass();
    if (objectClass.isAnnotationPresent(MyAnnotations.class)) {
      for (Field field : objectClass.getDeclaredFields()) {
        Annotation[] annotations = field.getAnnotations();
        for (Annotation annotation : annotations) {
          System.out.println(((MyAnnotations) annotation).annotationType());
        }
      }
    } else {
      throw new RuntimeException("CLass does not contain MyAnnotations");
    }
  }

  public static void main(String[] args) {
    Class objectClass = Customer.class;
    Field[] fields = objectClass.getDeclaredFields();
    for (Field field : fields) {
      Annotation[] annotations = field.getAnnotations();
      for (Annotation annotation : annotations) {
        System.out.println("annotations.getClass() = " + annotations.getClass());
      }
    }

  }
}
