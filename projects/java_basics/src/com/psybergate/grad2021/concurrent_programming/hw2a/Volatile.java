package com.psybergate.grad2021.concurrent_programming.hw2a;

// TODO: 2021/04/19 Not working as expected

public class Volatile {
  public volatile int sharedValue = 0;

  public static Volatile instance = new Volatile();

  public static void main(String[] args) throws InterruptedException {
    Thread thread = new Thread(new Worker());
    Thread thread1 = new Thread(new Worker());
    Thread thread2 = new Thread(new Worker());

    thread2.start();
    thread1.start();
    thread.start();

    thread2.join();
    thread1.join();
    thread.join();

    System.out.println("sharedValue = " + getInstance().sharedValue);
  }

  public int getSharedValue() {
    return sharedValue;
  }

  public void incrementSharedValue() {
    this.sharedValue += 1;
  }

  public static Volatile getInstance(){
    return instance;
  }
}

