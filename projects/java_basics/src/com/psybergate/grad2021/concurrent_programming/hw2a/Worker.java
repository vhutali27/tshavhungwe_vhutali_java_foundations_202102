package com.psybergate.grad2021.concurrent_programming.hw2a;

class Worker implements Runnable {

  Volatile aVolatile = Volatile.getInstance();

  @Override
  public void run() {
    for (int i = 0; i < 5000; i++) {
      aVolatile.incrementSharedValue();
    }
  }
}
