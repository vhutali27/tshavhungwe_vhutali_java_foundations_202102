package com.psybergate.grad2021.concurrent_programming.ce4a;

public class ThreadSafeSingleton {

  private static ThreadSafeSingleton threadSafeSingleton = new ThreadSafeSingleton();

  public static synchronized ThreadSafeSingleton getThreadSafeSingleton() {
    return threadSafeSingleton;
  }
}
