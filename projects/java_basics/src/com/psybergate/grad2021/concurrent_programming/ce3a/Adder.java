package com.psybergate.grad2021.concurrent_programming.ce3a;

public class Adder implements Operation {
  private long sum;
  private int start;
  private int end;

  public Adder(int start, int end) {
    this.start = start;
    this.end = end;
    calculateSum(start, end);
  }

  public long calculateSum(int start, int end) {
    long sum = 0;
    for (int i = start; i <= end; i++) {
      sum += i;
    }
    this.sum = sum;
    return sum;
  }

  public long calculateSum() {
    return this.calculateSum(this.start, this.end);
  }

  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  @Override
  public int setStart() {
    return 0;
  }

  @Override
  public int setEnd() {
    return 0;
  }

  public long getSum() {
    return sum;
  }

  @Override
  public long operate() {
    return calculateSum(this.start, this.end);
  }

  @Override
  public long getResult() {
    return this.getSum();
  }
}
