package com.psybergate.grad2021.concurrent_programming.ce3a;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NoThreadInterference {
  private static ExecutorService executor;
  private  static List<Worker> workers = new ArrayList<>();
  public static long stopConcurrent(){
    executor.shutdown();
    long result = 0;
    if (executor.isShutdown()){
      for (Worker worker : workers){
        if (worker.getOperation().getClass() == Adder.class) {
          result += worker.getResult();
        }
        else {
          if (result == 0){
            result = 1;
          }
          result *= worker.getResult();
        }
      }
      return result;
    }
    else{
      return 0;
    }
  }

  public static void spawnThread(Operation operation){
    Worker worker = new Worker(operation);
    workers.add(worker);
    Thread thread = new Thread(worker);
    executor.execute(thread);
  }

  public static long concurrentFactorial(int numThreads, int range){
    executor = Executors.newFixedThreadPool(numThreads);
    int value = range/numThreads;

    for (int i = 0; i < numThreads; i++) {
      Operation operation;
      if (i == numThreads - 1 && range % numThreads != 0) {
        operation = new Factorial(value * i + 1, range);

      } else {
        operation = new Factorial(value * i + 1, value * (i + 1));
      }
      spawnThread(operation);
    }
    return stopConcurrent();
  }

  public static long concurrentAdd(int numThreads, int range) {
    executor = Executors.newFixedThreadPool(numThreads);
    int value = range/numThreads;

    for (int i = 0; i < numThreads; i++) {
      Operation operation;
      if (i == numThreads - 1 && range % numThreads != 0) {
        operation = new Adder(value * i + 1, range);

      } else {
        operation = new Adder(value * i + 1, value * (i + 1));
      }
      spawnThread(operation);
    }
    return stopConcurrent();
  }

  public static void main(String[] args) {
    long sum = concurrentFactorial( 2, 5);
    System.out.println("concurrentAdder " + sum);
  }
}

