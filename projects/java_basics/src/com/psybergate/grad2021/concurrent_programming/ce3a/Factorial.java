package com.psybergate.grad2021.concurrent_programming.ce3a;

public class Factorial implements Operation {
  private long factorial;
  private int start;
  private int end;

  public Factorial(int start, int end) {
    this.start = start;
    this.end = end;
    calculateFactorial(start, end);
  }

  @Override
  public int getStart() {
    return start;
  }

  public void setStart(int start) {
    this.start = start;
  }

  @Override
  public int getEnd() {
    return end;
  }

  public void setEnd(int end) {
    this.end = end;
  }

  @Override
  public int setStart() {
    return 0;
  }

  @Override
  public int setEnd() {
    return 0;
  }

  public long calculateFactorial(int start, int end) {
    long factorial = 1;
    for (int i = start; i <= end; i++) {
      factorial *= i;
    }
    this.factorial = factorial;
    return factorial;
  }

  public long calculateFactorial(){
    return this.calculateFactorial(this.start, this.end);
  }

  public long getFactorial() {
    return factorial;
  }

  @Override
  public long operate() {
    return calculateFactorial(this.start, this.end);
  }

  @Override
  public long getResult() {
    return this.getFactorial();
  }
}
