package com.psybergate.grad2021.concurrent_programming.ce3a;

public class Worker implements Runnable {
  private int end;
  private long result;
  private int start;

  public Operation getOperation() {
    return operation;
  }

  private Operation operation;

  public Worker(Operation operation) {
    this.operation = operation;
  }

  @Override
  public void run() {
    this.result = operation.operate();
  }

  public long getResult() {
    return this.result;
  }
}
