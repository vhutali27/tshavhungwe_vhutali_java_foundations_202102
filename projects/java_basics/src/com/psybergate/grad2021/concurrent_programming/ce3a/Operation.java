package com.psybergate.grad2021.concurrent_programming.ce3a;

public interface Operation {

  public long operate();

  public long getResult();

  public int getStart();

  public int getEnd();

  public int setStart();

  public int setEnd();
}
