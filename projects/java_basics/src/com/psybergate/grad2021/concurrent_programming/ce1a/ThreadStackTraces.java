package com.psybergate.grad2021.concurrent_programming.ce1a;

import com.psybergate.grad2021.core.exceptions.class_exercise2.MyCheckedException;

public class ThreadStackTraces {

  public static void main(String[] args) {
    for (int i = 0; i < 4; i++){
      new MyThreads().start();
    }
  }
}

class MyThreads extends Thread{
  @Override
  public void run() {
    try {
      first();
    } catch (MyCheckedException e) {
      System.out.println("Thread ID: " + e.toString() + this.getName());
    }
  }

  public static void third() throws MyCheckedException {
    throw new MyCheckedException();
  }

  public static void second() throws MyCheckedException {
    third();
  }

  public static void first() throws MyCheckedException {
    second();
  }


}
