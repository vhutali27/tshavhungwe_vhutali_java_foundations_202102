package com.psybergate.grad2021.concurrent_programming.hw1;

class Wait implements Runnable {
  private MessageReaderWriter messageReaderWriter;

  public Wait(MessageReaderWriter messageReaderWriter) {
    this.messageReaderWriter = messageReaderWriter;
  }

  @Override
  public void run() {
    synchronized (messageReaderWriter) {
      System.out.println("Waiting for notify");
      try {
        messageReaderWriter.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      System.out.println("got notified");
      messageReaderWriter.readMessages();
    }

  }
}
