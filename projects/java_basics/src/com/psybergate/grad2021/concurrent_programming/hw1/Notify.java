package com.psybergate.grad2021.concurrent_programming.hw1;

import java.util.Random;

class Notify implements Runnable {
  private MessageReaderWriter messageReaderWriter;

  public Notify(MessageReaderWriter messageReaderWriter) {
    this.messageReaderWriter = messageReaderWriter;
  }

  private static String getRandomMessage() {
    Random random = new Random();
    int length = random.nextInt(15);
    String string = "";
    for (int i = 0; i < length; i++) {
      string += (char) (97 + random.nextInt(26));
    }
    return string;
  }

  @Override
  public void run() {
    System.out.println(Thread.currentThread().getName() + " Notifying");
    {
      synchronized (messageReaderWriter) {
        messageReaderWriter.writeMessage(getRandomMessage());
        messageReaderWriter.notify();
      }
    }
  }
}
