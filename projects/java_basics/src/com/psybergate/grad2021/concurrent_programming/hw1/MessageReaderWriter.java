package com.psybergate.grad2021.concurrent_programming.hw1;

import java.util.LinkedList;
import java.util.Queue;

public class MessageReaderWriter {
  private Queue<String> messages = new LinkedList<>();

  public static void main(String[] args) throws InterruptedException {
    MessageReaderWriter messageReaderWriter = new MessageReaderWriter();
    Wait wait = new Wait(messageReaderWriter);
    new Thread(wait, "wait").start();
    Notify notify = new Notify(messageReaderWriter);
    new Thread(notify, "notify").start();
    System.out.println("Thread started");
  }

  public synchronized void writeMessage(String message) {
    messages.add(message);
  }

  public void readMessages() {
    while (!messages.isEmpty()) {
      System.out.println("messages.poll() = " + messages.poll());
    }
  }
}

