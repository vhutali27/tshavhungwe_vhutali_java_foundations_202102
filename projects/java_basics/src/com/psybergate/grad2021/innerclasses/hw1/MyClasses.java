package com.psybergate.grad2021.innerclasses.hw1;

public class MyClasses {
  static class StaticInner{
    void print(){
      System.out.println("Static Inner Class");
    }
  }

  class ObjectInnerClass {
    void print(){
      System.out.println("Object Inner Class");
    }
  }

}
