package com.psybergate.grad2021.reflection.ce2;

import com.psybergate.grad2021.reflection.ce1.Student;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public class ReflectionUtil {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
    Class objectClass = Student.class;

    Constructor<Student> constructor = objectClass.getConstructor();
    Constructor<Student> constructor1 = objectClass.getConstructor(String.class);

    Student student = constructor.newInstance();
    Student student1 = constructor1.newInstance("Taylor");

    student1.setDegree("Comsci");
    System.out.println("student1 = " + student1.getDegree());
  }
}
