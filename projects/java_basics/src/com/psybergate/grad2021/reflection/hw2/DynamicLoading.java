package com.psybergate.grad2021.reflection.hw2;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Scanner;

public class DynamicLoading {
  public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, FileNotFoundException, ClassNotFoundException {

    File file = new File("C:\\myworkbench\\mymentoring\\java_foundations_202102\\projects\\java_basics\\src\\com" +
        "\\psybergate" +
        "\\grad2021\\reflection\\hw2\\implementation.txt");
    Scanner scanner = new Scanner(file);
    if (scanner.hasNext()) {
      String implementation = scanner.nextLine();
      System.out.println("implementation = " + implementation);
      Class objectClass = Class.forName(implementation);
      List<String> list = (List) objectClass.getConstructor().newInstance();

      list.add("January");
      list.add("February");
      list.add("March");
      list.add("April");
      list.add("May");
      list.add("June");
      list.add("July");
      list.add("August");
      list.add("September");
      list.add("October");
      list.add("November");
      list.add("December");


      for (String string : list) {
        System.out.println(string);
      }
    }
  }

}
