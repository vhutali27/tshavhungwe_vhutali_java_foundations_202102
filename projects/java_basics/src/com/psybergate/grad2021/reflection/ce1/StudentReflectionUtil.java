package com.psybergate.grad2021.reflection.ce1;

public class StudentReflectionUtil {



  public static <T> void printArray(T[] ts) {
    System.out.println("\nPrinting: " + ts.getClass());
    for (T t : ts) {
      System.out.println(t);
    }
  }

  public static void main(String[] args) {
    Class reflectedClass = Student.class;
    System.out.println("Class name : " + reflectedClass);
    System.out.println("\nPackage name : " + reflectedClass.getPackage());
    printArray(reflectedClass.getAnnotations());
    printArray(reflectedClass.getInterfaces());
    printArray(reflectedClass.getFields());
    printArray(reflectedClass.getDeclaredFields());
    printArray(reflectedClass.getDeclaredConstructors());
    printArray(reflectedClass.getDeclaredMethods());
  }
}
