package com.psybergate.grad2021.reflection.ce1;

import com.psybergate.grad2021.annotations.hw1.MyAnnotations;

import java.io.Serializable;

@MyAnnotations.DomainClass
public class Student implements Serializable {

  public static String name;

  private double height;

  private String degree;

  private String major;

  private int age;

  public Student() {
  }

  public Student(String name, String degree) {
    this.name = name;
    this.degree = degree;
  }

  public Student(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getHeight() {
    return height;
  }

  public void setHeight(double height) {
    this.height = height;
  }

  public String getDegree() {
    return degree;
  }

  public void setDegree(String degree) {
    this.degree = degree;
  }

  public String getMajor() {
    return major;
  }

  public void setMajor(String major) {
    this.major = major;
  }

  public int getAge() {
    return age;
  }

  public void setAge(int age) {
    this.age = age;
  }

  @Override
  public String toString() {
    return String.format("Name: %s\nAge: %d\nDegree: %s", this.name, this.age, this.degree);
  }

}
