package com.psybergate.grad2021.reflection.hw1;

import com.psybergate.grad2021.reflection.ce1.Student;

import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;


public class ReflectionUtil {
  public static <T> void printMemberDetails(Member[] members) {
    for (Member member : members) {
      System.out.println("member.getName() = " + member.getName());
      System.out.println("member.getModifiers() = " + (Modifier.toString(member.getModifiers())));
      System.out.println();
    }
  }

  public static void main(String[] args) {
    Class objectClass = Student.class;

    System.out.println("objectClass.getSuperclass() = " + objectClass.getSuperclass());

    System.out.println("\nPrinting Class modifiers");
    System.out.println(Modifier.toString(objectClass.getModifiers()));

    System.out.println("\nPrinting field modifiers");
    printMemberDetails(objectClass.getDeclaredFields());

    Method[] methods = objectClass.getDeclaredMethods();
    System.out.println("\nPrinting method modifiers");
    printMemberDetails(methods);

    System.out.println("\nPrinting method return types");
    for (Method method : methods){
      System.out.println("method.getName() = " + method.getName());
      System.out.println("method.getReturnType() = " + method.getReturnType());
      System.out.println();
    }
  }
}
