package com.psybergate.grad2021.core.langoverview.hw4a;

public class Operators {
    public static void main(String[] args) {
        int mod = 10 % 4;
//        System.out.println(mod);
        System.out.printf("10 mod 4 = %d\n", mod);

        // pre increment
        int i = 0;
        System.out.printf("pre increment %d\n", ++i);

        // post increment
        i = 0;
        System.out.printf("Post increment %d\n", i++);

        // == primitive
        System.out.printf("Primitive 1 == 1: %b\n", 1 == 1);
        System.out.printf("Primitive 129 == 129: %b\n", 129 == 129);

        // == Object
        System.out.printf("Object 1 == 1: %b\n", new Integer(1) == new Integer(1));
        System.out.printf("Object 129.equals(129): %b\n", new Integer(129).equals(129));

        // Not sure how to illustrate the following two
        // & Bitwise AND: returns the bitwise and result of a and b
        System.out.printf("false & true: %b\n", false & true);

        // &&  Conditional AND, evaluates b if a is true in (a && b)
        System.out.printf("false && true: %b\n", false && true);

        // | Bitwise OR
        System.out.printf("true | false: %b\n", true | false);

        // || Conditional OR, evaluates b only if a is false in (a || b)
        System.out.printf("true || false: %b\n", true || false);

        // +=
        i = 0;
        i += 2;
        System.out.printf("i += 2: %d\n", i);

        // tenary operator
        i = (i == 2) ? 3 : 4;
        System.out.printf("i = (i == 2) ? 3 : 4 => i: %d\n", i);

        // switch
        switch (i){ // arg can only be int, byte, short, char, string, enun
            case 1: // case must be constant or a literal of same data type
                System.out.printf("switch i = %d\n", 1);
                break;
            case 2:
                System.out.printf("switch i = %d\n", 2);
                break;
            case 3:
                System.out.printf("switch i = %d\n", 3); // i == 3\
                // All cases after this will be evaluated, unless there is a break
                break;
            case 4:
                System.out.printf("switch i = %d\n", 4);
                break;
            default: // optional
                System.out.printf("switch i not found");
        }
    }
}
