package com.psybergate.grad2021.core.langoverview.hw6a;

public class StaticModifier {


    public static void main(String[] args) {
        StaticModifierClass instance = new StaticModifierClass();

        System.out.println("First Instance");
        System.out.println(instance.getInstanceVariable());
        System.out.println(StaticModifierClass.getInstanceCounter());

        StaticModifierClass instance2 = new StaticModifierClass();
        System.out.println("Second Instance");
        System.out.println(instance2.getInstanceVariable());
        System.out.println(StaticModifierClass.getInstanceCounter());
    }
}

class StaticModifierClass {
    private static int instanceCounter = 0;
    private int instanceVariable = 0;

    StaticModifierClass(){
        StaticModifierClass.instanceCounter++;
        this.instanceVariable++;
    }

    public static int getInstanceCounter(){
        return StaticModifierClass.instanceCounter;
    }
    public int getInstanceVariable(){
        return this.instanceVariable;
    }

}