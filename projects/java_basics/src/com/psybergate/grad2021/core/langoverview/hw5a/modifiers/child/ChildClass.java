package com.psybergate.grad2021.core.langoverview.hw5a.modifiers.child;

import com.psybergate.grad2021.core.langoverview.hw5a.modifiers.AccessModifiers;

public class ChildClass extends AccessModifiers {
    public static void childClass(){
        protectedFunc();
        publicFunc();
    }
}
