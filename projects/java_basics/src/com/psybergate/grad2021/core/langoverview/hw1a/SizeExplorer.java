package com.psybergate.grad2021.core.langoverview.hw1a;

public class SizeExplorer {

    public static void main(String[] args) {

        System.out.printf("%s\nMin: %d\tMax:%d\tlog2: %d\n\n", Byte.class, Byte.MIN_VALUE, Byte.MAX_VALUE, Log2.log2(Byte.MAX_VALUE));
        System.out.printf("%s\nMin: %d\tMax:%d\tlog2: %d\n\n", Short.class, Short.MIN_VALUE, Short.MAX_VALUE, Log2.log2(Short.MAX_VALUE));
        System.out.printf("%s\nMin: %d\tMax:%d\tlog2: %d\n\n", Integer.class, Integer.MIN_VALUE, Integer.MAX_VALUE, Log2.log2(Integer.MAX_VALUE));
        System.out.printf("%s\nMin: %d\tMax:%d\tlog2: %d\n\n", Long.class, Long.MIN_VALUE, Long.MAX_VALUE, Log2.log2(Long.MAX_VALUE));
    }
}

class Log2 {
    public static int log2(Number n){
        double N = Double.parseDouble(n.toString());
        return (int) (Math.log(N)/Math.log(2) + 1);
    }
}
