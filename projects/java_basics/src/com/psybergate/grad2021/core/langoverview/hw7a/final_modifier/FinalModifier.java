package com.psybergate.grad2021.core.langoverview.hw7a.final_modifier;

final class FinalModifier { // cannot be extended
    private final String name; // Unique to each instance

    public final int id; // Can either be initialised here or assigned in the constructor

    private final void privateFinalMethod(){
        System.out.println("private final, I cannot be overridden");
    }

    public final void publicFinalMethod(final String str){
        // str cannot be changed after this
        System.out.println("public final I cannot be overridden");
    }

    // Constructor cannot be overridden
    FinalModifier(String name, int id){
        this.name = name;
        this.id = id;
    }
}
