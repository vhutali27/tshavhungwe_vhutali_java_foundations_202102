package com.psybergate.grad2021.core.langoverview.hw5a.modifiers;

public class PackageLevel {

    public static void packageLevel() {
        com.psybergate.grad2021.core.langoverview.hw5a.modifiers.AccessModifiers.packageFunc();
        com.psybergate.grad2021.core.langoverview.hw5a.modifiers.AccessModifiers.protectedFunc();
        com.psybergate.grad2021.core.langoverview.hw5a.modifiers.AccessModifiers.publicFunc();
    }
}
