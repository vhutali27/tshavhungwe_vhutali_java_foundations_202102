package com.psybergate.grad2021.core.langoverview.hw5a.modifiers;

import com.psybergate.grad2021.core.langoverview.hw5a.modifiers.child.ChildClass;
import com.psybergate.grad2021.core.langoverview.hw5a.outlier.OutlierClass;

public class AccessModifiers {

    // All function are static so you can access them in main
    private static void privateFunc(){
        System.out.println("Private Function");
    }

    static void packageFunc(){
        System.out.println("Package Function");
    }

    protected static void protectedFunc(){
        System.out.println("Protected Function");
    }

    public static void publicFunc(){
        System.out.println("public Function");
    }

    public static void main(String[] args) {
        System.out.println("From AccessModifiers Class:");
        privateFunc();
        packageFunc();
        protectedFunc();
        publicFunc();
        System.out.println("\nFrom PackageLevel Class:");

        PackageLevel.packageLevel();

        System.out.println("\nFrom Child Class");
        ChildClass.childClass();

        System.out.println("\nFrom Outlier Class");
        OutlierClass.outlierClass();
    }
}
