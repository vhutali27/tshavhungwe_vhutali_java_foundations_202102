/*
    Class loading os the process of storing class-specific information in memory
    Class information means, information about the class members, i.e., varibales and tmethods
    Static blocks run only once in the life of a class
    They can only access the static members and will only belong to the class
    The JVM executes static blocks at "CLASS LOADING TIME"

    Execution order
    - Static block
    - Static variable
    - Static method
 */

package com.psybergate.grad2021.core.langoverview.hw2a;

public class ClassLoadingHomeWork {
    static String var = "Static Object Variable";

    ClassLoadingHomeWork(){
        System.out.println("Constructor"); // Instance
    }

    public static void main(String[] args) {
        System.out.println("Static Function");
    }

    static {
        String Local = "Block scope string";
        System.out.println("Static block");
        System.out.println(var); // TODO: 2021/02/08 I expected this to not print static variable, go do some more reading
        // I.e. If the blcok is running first, the assignment shouldn't be there right?
    }
}
