
package com.psybergate.grad2021.enums;

enum Students {
  TYLER(20, 1.72),
  TAYLORT(22, 1.6),
  SELENA(27, 1.65),
  JAIME(21, 1.64),
  MARK;

  private int age;

  private double height;

  private Students(){};

  private Students(int age, double height){
    this.age = age;
    this.height = height;
  }

  public int getAge() {
    return age;
  }

  public double getHeight() {
    return height;
  }

}

public class StudentsEnum{
  public static void main(String[] args) {
    System.out.println(Students.TYLER.getAge());
    System.out.println(Students.JAIME.getHeight());
  }
}


