package com.psybergate.grad2021.enums;

public final class SimulatedEnums {

  public static final SimulatedEnums TYLER = new SimulatedEnums("Tyler", 22, 1.72);
  public static final SimulatedEnums DEMI = new SimulatedEnums("Selena", 25, 1.66);
  public static final SimulatedEnums SELENA = new SimulatedEnums("Demi", 27, 1.55);


  private final String name;

  private final int age;

  private final double height;

  private SimulatedEnums(String name, int age, double height) {
    this.name = name;
    this.age = age;
    this.height = height;
  }

  public int getAge() {
    return age;
  }

  public double getHeight() {
    return height;
  }

  public static void main(String[] args) {
    System.out.println(SimulatedEnums.DEMI.getAge());
    System.out.println(SimulatedEnums.SELENA.getHeight());
  }
}
