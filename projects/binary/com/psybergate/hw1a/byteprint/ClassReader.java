package com.psybergate.hw1a.byteprint;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


public class ClassReader {

    public static void main(String[] args) throws IOException {
            try {
                Path classFile = Paths.get("C:\\myworkbench\\myprojects\\Binary\\bin\\hw1a\\ClassReader.class");
                byte[] data = Files.readAllBytes(classFile);
                for (int i = 0; i < data.length; i++){
                    if (i % 2 == 0)
                        System.out.printf(" ");
                    System.out.printf("%02x", data[i]);
                }
            }
            catch (IOException e) {
                e.printStackTrace();
            }
    }
}