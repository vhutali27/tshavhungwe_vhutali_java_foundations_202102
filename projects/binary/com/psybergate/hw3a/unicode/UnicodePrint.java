// TODO: 2021/02/07 Haven't implemented unicode print

package com.psybergate.hw3a.unicode;

import java.io.PrintStream;
import java.nio.charset.StandardCharsets;

public class UnicodePrint {
    public static void main(String[] args) {
        for (int i = 0; i < 1000; i++){
            System.out.printf("%02x", String.valueOf(i).getBytes(StandardCharsets.UTF_16));
        }
    }
}
