package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.time.LocalDate;

public class Customer implements Comparable{
  private static int numberOfCustomers = 0;

  private String customerName;

  private String customerNumber;

  private LocalDate customerRegistrationDate;

  private String locality = "Local"; // Defualt is Local

  public Customer(String customerName){
    this(customerName, "Local", LocalDate.now());
  }

  public Customer(String customerName, String locality){
    this(customerName, locality, LocalDate.now());
  }

  public Customer(String customerName, LocalDate customerRegistrationDate){
    this(customerName, "Local", customerRegistrationDate);
  }

  public Customer(String name, String locality, LocalDate customerRegistrationDate){
    this.customerName = name;
    this.customerNumber = allocateCustomerNum();
    this.locality = locality;
    this.customerRegistrationDate = customerRegistrationDate;
  }


  public String getCustomerName() {
    return customerName;
  }

  public String getCustomerNumber() {
    return this.customerNumber;
  }

  public LocalDate getCustomerRegistrationDate() {
    return customerRegistrationDate;
  }

  public String getLocality() {
    return locality;
  }

  @Override
  public String toString() {
    return this.customerName;
  }

  private static String allocateCustomerNum() {
    Customer.numberOfCustomers += 1;
    return Integer.toString(Customer.numberOfCustomers);
  }


  public int compareTo(Object obj){
    return (this.customerNumber.compareTo(((Customer)obj).getCustomerNumber()));
  }
}
