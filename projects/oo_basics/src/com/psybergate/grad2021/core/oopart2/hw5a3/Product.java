package com.psybergate.grad2021.core.oopart2.hw5a3;

public class Product {
  private String productName;
  private double price;
  private String description;

  public Product(String productName, double price){
    this.productName = productName;
    this.price = price;
  }

  public double getPrice(){
    return price;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  @Override
  public String toString() {
    return "Product{" +
        "productName='" + productName + '\'' +
        ", price=" + price +
        ", description='" + description + '\'' +
        '}';
  }
}
