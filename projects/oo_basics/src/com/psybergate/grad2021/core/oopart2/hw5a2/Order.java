package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
  // TODO: 2021/02/26 You need a unique identifier e.g. orderNum 
  protected final Customer customer;
  protected final LocalDate orderDate;
  protected List<OrderItem> orderItems;
  protected double orderCosts; // TODO: 2021/02/26 Derived value, you can just compute it instead of storing it

  // TODO: 2021/02/26 Your constructors need to have scope, use public, because now you have package
  Order(List<OrderItem> orderItems, Customer customer) {
    this(orderItems, customer, calcCosts(orderItems, 0));
  }

  Order(List<OrderItem> orderItems, Customer customer, double orderCosts) {
    this.orderItems = new ArrayList<>(orderItems);
    this.customer = customer;
    this.orderDate = LocalDate.now();
    this.orderCosts = orderCosts;
  }

  public static double calcCosts(List<OrderItem> orderItems, double discount) {
    double sum = 0;
    for (OrderItem orderItem : orderItems) {
      sum += orderItem.getOrderItemTotalCosts();
    }
    return Math.round(sum * (1 - discount));
  }

  public String getOrderDetails() {
    return "Order{" +
        "customer=" + this.customer +
        ", orderItems=" + this.getOrderItemDetails() +
        ", orderCosts=" + this.orderCosts +
        ", orderDate=" + this.orderDate +
        '}';
  }

  public Customer getCustomer() {
    return customer;
  }

  public double getOrderCosts() {
    return orderCosts;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public String getOrderItemDetails() {
    String orderItemDetails = "";
    for (OrderItem orderItem : this.orderItems) {
      orderItemDetails = orderItemDetails.concat(orderItem.getOrderSummary());
    }
    return orderItemDetails;
  }
}

// TODO: 2021/02/26 Separate files
class LocalOrder extends Order {

  LocalOrder(List<OrderItem> orderItems, Customer customer) {
    super(orderItems, customer, calcCosts(orderItems, customer));
  }

  public static double calcCosts(List<OrderItem> orderItems, Customer customer) {
    double discount = 0;

    // TODO: 2021/02/25 account for months and days
    int currentYear = LocalDate.now().getYear();
    int signUpYear = customer.getSignUpDate.getYear(); // TODO: 2021/02/26 use getter function

    if (currentYear - signUpYear > 5) {
      discount = 0.125;
    } else if (currentYear - signUpYear > 2) {
      discount = 0.075;
    }
    return calcCosts(orderItems, discount);
  }
}

class InternationalOrder extends Order {
  protected static double importDuty = (-1) * 0.15; // Just going with a standard 15%

  InternationalOrder(List<OrderItem> orderItems, Customer customer) {
    super(orderItems, customer, calcCosts(orderItems));
  }

  public static double calcCosts(List<OrderItem> orderItems) {
    double costs = calcCosts(orderItems, 0); // Original price, discount price
    if (costs > 1000000) {
      return costs * (0.9);
    } else if (costs > 500000) {
      return costs * (0.95);
    }
    return Math.round(costs * (1 + importDuty));
  }
}
