package com.psybergate.grad2021.core.oopart2.ce2a;

public class ParameterPassing {
  private Integer passByReference = new Integer(1);

  private static void passByValue(int b) {
    b += 1;
  }

  private static void passByReference(ParameterPassing b){
    b.passByReference += 1;
  }

  public static void main(String[] args) {

    int a = 1;
    passByValue(a);
    System.out.println(a);

    ParameterPassing param = new ParameterPassing();
    passByReference(param);
    System.out.println(param.passByReference);
  }
}

class SwapWrapper {
  private Object temp;
  // TODO: 2021/02/19 swap method
  // Once you copy it you create a new object, so a new object reference and therefore aren't
  // swapping the references


}
