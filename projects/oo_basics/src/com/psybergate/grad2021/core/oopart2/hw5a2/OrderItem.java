package com.psybergate.grad2021.core.oopart2.hw5a2;

public class OrderItem {

  private static int numberoforders = 0;
  // Fields found from https://schema.org/OrderItem
  private final Customer customer;
  private final Product orderItemProduct;
  private final int orderItemNumber;
  private int orderItemQuantity; // My understanding is and OrderItem is for one Product type, however, there is no limit on
  private final String orderItemStatus;
  private String orderDelivery;
  private final double orderItemTotalCosts;

  OrderItem(Customer customer, Product orderItemProduct) {
    this(customer, customer.getAddress(), orderItemProduct, 1);
  }

  OrderItem(Customer customer, Product orderItemProduct, int orderItemQuantity) {
    this(customer, customer.getAddress(), orderItemProduct, orderItemQuantity);
  }

  OrderItem(Customer customer, String orderDelivery, Product orderItemProduct) {
    this(customer, orderDelivery, orderItemProduct, 1);
  }

  OrderItem(Customer customer, String orderDelivery, Product orderItemProduct, int orderItemQuantity) {
    this.customer = customer;
    this.orderDelivery = orderDelivery;
    // TODO: 2021/02/25 Look into creating a generator
    this.orderItemNumber = numberoforders;
    numberoforders++;
    this.orderItemStatus = "Processing";
    this.orderItemProduct = orderItemProduct;
    this.orderItemQuantity = orderItemQuantity;
    this.orderItemTotalCosts = orderItemProduct.getProductPrice() * orderItemQuantity;
  }

  public Product getOrderItemProduct() {
    return this.orderItemProduct;
  }

  public double getOrderItemTotalCosts() {
    return this.orderItemTotalCosts;
  }

  public Customer getCustomer() {
    return this.customer;
  }

  public String getOrderItemStatus() {
    return this.orderItemStatus;
  }

  public String getOrderSummary() {
    return String.format("Order item number: %s\nStatus: %s\nProduct: %s\n\n",
        this.orderItemNumber, this.getOrderItemStatus(), this.orderItemProduct.getProductName());
  }

  public String getOrderDetails() {
    return String.format("Order Summary: %s\nProduct Details: %s\nQuantity: %s\nDelivery Address: %s\n\n",
        this.getOrderSummary(), this.orderItemProduct.getProductDetails(), this.orderItemQuantity,
        this.customer.getAddress());
  }
}
