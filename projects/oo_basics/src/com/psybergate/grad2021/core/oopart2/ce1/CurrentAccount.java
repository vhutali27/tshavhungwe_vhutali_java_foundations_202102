package com.psybergate.grad2021.core.oopart2.ce1;

public class CurrentAccount extends Account {
    private double overDraft;
    private double limit; // overDraft + 0.2*overDraft
    /*
    Constructors are not inherited because the compiler takes it as just an ordinary method in the child class
    and therefore expects a return type
     */

    public CurrentAccount(String accountNum, String accountHolder, double overDraft, double limit) {
        super(accountNum, accountHolder);
        this.overDraft = overDraft;
        this.limit = overDraft;
        System.out.printf("In CurrentAccount:\n\toverDraft: %f\n\tlimit: %f\n\n", this.overDraft, this.limit);
    }

    @Override
    public boolean equals(Object account) {
        return this.accountNum.equals(((CurrentAccount) account).accountNum);
    }

    public static void main(String[] args) {
        CurrentAccount ca = new CurrentAccount("1", "Tyler May", 10000, 12000);
    }
}
