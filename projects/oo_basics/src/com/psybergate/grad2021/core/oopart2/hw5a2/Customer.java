package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  private static int numOfCustomers = 1; // You initially it had in uppercase

  public LocalDate getSignUpDate;

  private String name;
  private String customerID;
  private boolean international; // Used to mark internation customers
  // if something is not logically implied in terms of boolean, do not model it with a boolean
  // Rather use a type, using for example a String, once you are comfortable with enums, you should use those
  private LocalDate signUpDate;
  private String address;

  private List<OrderItem> cart = new ArrayList<>();
  private List<Order> orders = new ArrayList<>();
  // TODO: 2021/02/26 You have a dual relationship, you shouldn't have that


  // Constructor if name is just given, local is assumed, current date is assumed
  public Customer(String name) {
    this(name, false);
  }

  // Constructor with the ability to distinguish between internation and local, current date is assumed
  public Customer(String name, boolean international) {
    this(name, international, LocalDate.now());
  }

  // Locality is assumed
  public Customer(String name, LocalDate signUpDate) {
    this(name, false, signUpDate);
  }

  // Object Constructor
  public Customer(String name, boolean international, LocalDate signUpDate) {
    this.name = name;
    this.international = international;
    this.signUpDate = signUpDate;
    this.customerID = name + numOfCustomers;
    numOfCustomers += 1; // Increase the total number of customers
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    // Set afterwards because you want to make the "sign up" process as quick as possible to get the user to start
    // using the service
    this.address = address;
  }

  public List<Order> getOrders() {
    return orders;
  }

  public boolean isInternational() {
    return international;
  }


  // TODO: 2021/02/26 Create a simple abstraction
  // TODO: 2021/02/26 You can add the concept of a cart in a later version 
  // TODO: 2021/02/24 This should be in a different class
  public void addToCart(Product product) {
    this.addToCart(this.getAddress(), product, 1);
  }

  public void addToCart(Product product, int quantity) {
    this.addToCart(this.getAddress(), product, quantity);
  }

  public void addToCart(String address, Product product, int quantity) {
    product.purchase(quantity);
    this.cart.add(new OrderItem(this, address, product, quantity));
  }

  public void checkOut() {
    if (this.isInternational()) {
      this.orders.add(new InternationalOrder(this.cart, this));
    } else {
      this.orders.add(new LocalOrder(this.cart, this));
    }
    this.cart.clear();
  }

}
