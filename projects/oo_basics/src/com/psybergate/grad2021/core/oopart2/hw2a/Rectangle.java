package com.psybergate.grad2021.core.oopart2.hw2a;


import org.w3c.dom.css.Rect;

import java.util.Objects;

public class Rectangle {
  // static variables
  private static final String LIMITS = "required: length > width > 0; length <= 250, width <= 150, area <= 15000";
  // size
  protected final float length;
  protected final float width;
  protected final float area;

  Rectangle(float length, float width) {
    if (valid(length, width)) {
      this.length = length;
      this.width = width;
      this.area = length * width;
    } else {
      throw new RuntimeException(String.format("Cannot create instance of Rectangle due to invalid " +
          "parameters; %s; passed: length " +
          "= " +
          "%f, " +
          "width = %f, " +
          "area = %f", LIMITS, length, width, length * width));
    }
  }

    /*
     Static Methods
     */

  public static boolean valid(float length, float width) {
    return (width > 0) && (length > width) && (length <= 250) && (width <= 150) && (length * width <= 15000);
  }

  @Override
  public boolean equals(Object o) {
    // TODO: 2021/02/19 check for compliance with the other rules mentioned in the side
    if (this == (Rectangle) o) // Identity ?
      return true;

    // Code along with chris not your own work
    if (o == null) {
      return false;
    }

    // Code along with Chris
    if (!(o instanceof  Rectangle)){
      return false;
    }

    // Your own code
    Rectangle rectangle = (Rectangle) o;
    return Float.compare(rectangle.length, length) == 0 && Float.compare(rectangle.width, width) == 0;
  }

  public static void main(String[] args) {
    Rectangle rectangle = new Rectangle(6, 5);
    Rectangle rectangle1 = new Rectangle(6, 5);

    System.out.println(rectangle == rectangle1);
    System.out.print(rectangle.equals(rectangle1));
  }
}
