package com.psybergate.grad2021.core.oopart2.hw3a;

import java.util.ArrayList;
import java.util.List;


public class Customer {

  private static int NUMCUSTOMERS = 100000;
  // TODO: 2021/02/12 You have 6 object attributes required no more than 5
  private final boolean isPerson; // type of customer: Person or Company
  private final String customerNum; // customer number
  private final String name;
  private final List<String> purchaseHistory = new ArrayList<>();
  private final List<String> placedOrders = new ArrayList<>();
  private int age; // Alternatively you could preferably parse the date of birth

  // for companies
  Customer(String companyName, int age) {
    this(false, companyName, age);
  }

  Customer(Boolean isPerson, String customerName, int age) {
    if (isPerson) {
      assert (age > 17) : "Underage, must be 18 or older";
      this.age = age;
    }
    this.customerNum = getCustomerNum();
    this.isPerson = isPerson;
    this.name = customerName;

  }

  /*
  Static method getCustomerNumber
  returns a unique customer identifier number
   */
  private static String getCustomerNum() {
    NUMCUSTOMERS++;
    return Integer.toString(NUMCUSTOMERS);
  }

  public static void main(String[] args) {
    Customer person1 = new Customer(true, "Tyler May", 27);
    Customer business1 = new Customer("Company", 5);
    Customer business2 = new Customer(false, "Corporate", 90);

    System.out.println("person1.equals(person1) = " + person1.equals(person1));
    System.out.println("person1.equals(business1) = " + person1.equals(business1));
    System.out.println("person1.equals(null) = " + person1.equals(null));
  }

  public boolean equals(Object obj) {
    // TODO: 2021/02/25 Make it so that you can only compare Customers and not subtypes
    //
    //  You can use getClass()
    //  .class
    //  .forName()
    //
    if (obj instanceof Customer) {
      if (this == obj) {
        return true;
      }
      return this.customerNum.equals(((Customer) obj).customerNum);
    }
    return false;
  }

  public int compareTo(Object obj){
    return (this.customerNum.compareTo(((Customer)obj).customerNum));
  }
}
