package com.psybergate.grad2021.core.oopart2.hw5a2;

public class Product {
  private final String productName;
  private int productQuantity; // TODO: 2021/02/25 move into seperate class
  private double productPrice;
  private String productDescription;


  Product(String productName, int productQuantity, double productPrice) {
    this.productName = productName;
    this.productQuantity = productQuantity;
    this.productPrice = productPrice;
  }

  public String getProductName() {
    return this.productName;
  }

  public String getProductDetails() {
    return String.format("Product name: %s\nPrice: %s\nQuantity: %s\nProduct Description: %s\n\n", this.productName,
        this.productPrice,
        this.productQuantity, this.productDescription);
  }

  public double getProductPrice() {
    return this.productPrice;
  }

  public void addProductDescription(String description) {
    this.productDescription = description;
  }

  public void updatePrice(double price) {
    this.productPrice = price;
  }

  public void increaseQuantity(int quantity) {
    this.productQuantity += quantity;
  }


  // TODO: 2021/02/25 Move this into a separate class
  public void purchase(int quantity) {
    if (this.productQuantity == 1) {
      throw new RuntimeException("Out of stock");
    }
    if (this.productQuantity < quantity) {
      throw new RuntimeException(String.format("Insufficient stock, available stock is: %d", this.productQuantity));
    } else {
      this.productQuantity -= quantity;
    }
  }

}
