package com.psybergate.grad2021.core.oopart2.demo1a;

public class Square extends Shape {
/*
Compiler treats this an ordinary method and will therefore expect a return type
*/
//    @Override
//    public Shape {
//
//    }

  public Square() {
    super();
  }
    /*
    Static method does appear as a method from the parent class, and therefore it is not possible to override
     */
//    @Override
//    public static void printHello(){
//        System.out.println("Hello World");
//    }

  public static void main(String[] args) {
    printHello();


  }

  @Override
  public void printSomething() {
    super.printSomething();
  }


}
