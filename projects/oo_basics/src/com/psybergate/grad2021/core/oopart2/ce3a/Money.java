package com.psybergate.grad2021.core.oopart2.ce3a;

import java.math.BigDecimal;

public final class Money {
    private BigDecimal sum;

    Money(int fee){
        this.sum = new BigDecimal(fee);
    }

    Money(double fee){
        this.sum = new BigDecimal(fee);
    }

    public BigDecimal add(Money money){
        return this.sum.add(money.sum);
    }

    public static void main(String[] args) {
        Money money1 = new Money(100000);
        Money money2 = new Money(100000.00);
        System.out.println(money1.add(money2));
    }
}
