package com.psybergate.grad2021.core.oopart2.hw5a3;

public class OrderItem {

  private final Product orderItemProduct;
  private final int orderItemQuantity;

  public OrderItem(Product orderItemProduct){
    this(orderItemProduct, 1);
  }

  public OrderItem(Product orderItemProduct, int orderItemQuantity){
    this.orderItemProduct = orderItemProduct;
    this.orderItemQuantity = orderItemQuantity;
  }

  public double getTotalCosts(){
    return this.orderItemProduct.getPrice() * this.orderItemQuantity;
  }


}
