package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.time.LocalDate;
import java.util.ArrayList;
//import java.util.HashMap;
import java.util.Arrays;
import java.util.List;

public class Order {


  protected final Customer customer;

  protected final String orderNum;

  protected final List<OrderItem> orderItemList;

  protected final LocalDate orderDate;


  protected static List<Order> ordersList = new ArrayList<>();
//  protected static Map<Customer, List<Order>> customerListMap = new HashMap<>();

  private static int numOrders = 0;

  public Order(Customer customer, OrderItem orderItem){
    this(customer, new ArrayList<>(Arrays.asList(orderItem)), 0);

  }

  public Order(Customer customer, List<OrderItem> orderItems, double discount){
    this.customer = customer;
    this.orderItemList = orderItems;
    this.orderDate = LocalDate.now();
    this.orderNum = allocateOrderNum();
    addOrder(this);
  }

  public void addOrderItem(OrderItem orderItem){
    this.orderItemList.add(orderItem);
  }

  public double getCosts(){
    double costs = 0;
    for (OrderItem orderItem : this.orderItemList){
      costs += orderItem.getTotalCosts();
    }
    return costs;
  }

  public Customer getCustomer() {
    return customer;
  }

  public List<OrderItem> getOrderItemList() {
    return orderItemList;
  }

  public String getOrderDetails(){
    // TODO: 2021/02/26 implement
    return null;
  }

  @Override
  public String toString() {
    return "Order{" +
        "customer=" + customer +
        ", orderItemList=" + orderItemList +
        ", orderCosts=" + this.getCosts() +
        ", orderDate=" + orderDate +
        '}';
  }

  public static List<Order> getCustomerOrders(Customer customer){
    List<Order> customerOrders = new ArrayList<>();

    for (Order order : Order.getAllOrders()){
      if (order.getCustomer().equals(customer)) {
        customerOrders.add(order);
      }
    }
    return customerOrders;
  }

  public static List<Order> getAllOrders(){
    return Order.ordersList;
  }

  private static void addOrder(Order order){
    Order.ordersList.add(order);
  }

  private static String allocateOrderNum(){
    Order.numOrders += 1;
    return Integer.toString(Order.numOrders);
  }
}
