package com.psybergate.grad2021.core.oopart2.hw5a;

// TODO: 2021/02/26 set your format to put a new line between variables


public class OrderItem {

  // TODO: 2021/02/26 if it was final you'd use an underscore as a separator 
  private static int NUMBEROFORDERS = 0; // TODO: 2021/02/26 you don't need to track everything 

  // TODO: 2021/02/26 It's better to ask before going on google 
  // Fields found from https://schema.org/OrderItem
  private Customer customer;
  private String orderDelivery; // TODO: 2021/02/26 This belongs in Order 
  private int orderItemNumber;
  private String status;
  private Product product;
  private int quantity; // My understanding is and OrderItem is for one Product type, however, there is no limit on
  // quantity?
  private double totalCosts; // TODO: 2021/02/26 Derived value use a method, also be consistent with your naming
  // TODO: 2021/02/26 Brittle code, if the quantity is changed then you need to reevaluate totalCosts 

  OrderItem(Customer customer, Product product) {
    this(customer, customer.getAddress(), product, 1);
  }

  OrderItem(Customer customer, Product product, int quantity) {
    this(customer, customer.getAddress(), product, quantity);
  }

  OrderItem(Customer customer, String orderDelivery, Product product) {
    this(customer, orderDelivery, product, 1);
  }

  OrderItem(Customer customer, String orderDelivery, Product product, int quantity) {
    this.customer = customer;
    this.orderDelivery = orderDelivery;
    this.orderItemNumber = NUMBEROFORDERS;
    NUMBEROFORDERS++;
    this.status = "Processing";
    this.product = product;
    this.quantity = quantity;
    this.totalCosts = product.getPrice() * quantity;
  }

  public Product getProduct() {
    return this.product;
  }

  public double getTotalCosts() {
    // TODO: 2021/02/28 implement
    return 0.0;
  }

  public Customer getCustomer() {
    return customer;
  }

  // TODO: 2021/02/26 You don't need an individual status for each OrderItem  
  public String getStatus() {
    return this.status;
  }

  public String getOrderSummary() {
    return String.format("Order item numebr: %s\nStatus: %s\nProduct: %s\n\n",
        orderItemNumber, this.getStatus(), product.getName());
  }

  public String getOrderDetails() {
    return String.format("Order Summary: %s\nProduct Details: %s\nDelivery Address: %s\n\n",
        this.getOrderSummary(), product.getProductDetails(), customer.getAddress());
  }
}
