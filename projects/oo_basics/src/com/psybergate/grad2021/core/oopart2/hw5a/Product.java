package com.psybergate.grad2021.core.oopart2.hw5a;

public class Product {
  private String name;
  private int quantity;
  private double price;
  private String description;


  Product(String name, int quantity, double price) {
    this.name = name;
    this.quantity = quantity;
    this.price = price;
  }

  public String getName() {
    return this.name;
  }

  public String getProductDetails() {
    return String.format("Product name: %s\nPrice: %s\nQuantity: %s\nProduct Description: %s\n\n", this.name,
        this.price,
        this.quantity, this.description);
  }

  public double getPrice() {
    return this.price;
  }

  public void addProductDescription(String description) {
    this.description = description;
  }

  public void updatePrice(double price) {
    this.price = price;
  }

  public void increaseQuantity(int quantity) {
    this.quantity += quantity;
  }

  public void purchase(int quantity) {
    if (this.quantity == 1) {
      throw new RuntimeException("Out of stock");
    }
    if (this.quantity < quantity) {
      throw new RuntimeException(String.format("Insufficient stock, available stock is: %d", this.quantity));
    } else {
      this.quantity -= quantity;
    }
  }

}
