package com.psybergate.grad2021.core.oopart2.hw5a;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Customer {

  // static variables
  protected static int NUMCUSTOMERS = 1;

  // Class variables
  protected boolean international; // Used to mark internation customers
  protected String name;
  protected String customerID;
  protected LocalDate signUpDate;
  protected String address;

  protected List<OrderItem> cart = new ArrayList<>();
  protected List<Order> orders = new ArrayList<>();


  // Constructor if name is just given, local is assumed, current date is assumed
  public Customer(String name) {
    this(name, false);
  }

  // Constructor with the ability to distinguish between internation and local, current date is assumed
  public Customer(String name, boolean international) {
    this(name, international, LocalDate.now());
  }

  // Locality is assumed
  public Customer(String name, LocalDate signUpDate) {
    this(name, false, signUpDate);
  }

  // Object Constructor
  public Customer(String name, boolean international, LocalDate signUpDate) {
    this.name = name;
    this.international = international;
    this.signUpDate = signUpDate;
    this.customerID = name + NUMCUSTOMERS;
    NUMCUSTOMERS += 1; // Increase the total number of customers
  }

  public String getName() {
    return name;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    // Set afterwards because you want to make the "sign up" process as quick as possible to get the user to start
    // using the service
    this.address = address;
  }
  public boolean isInternational() {
    return international;
  }
  // TODO: 2021/02/24 This could be in a different class
  public void addToCart(Product product) {
    this.addToCart(this.getAddress(), product, 1);
  }

  public void addToCart(Product product, int quantity) {
    this.addToCart(this.getAddress(), product, quantity);
  }

  public void addToCart(String address, Product product, int quantity) {
    product.purchase(quantity);
    this.cart.add(new OrderItem(this, address, product));
  }



  public void checkOut() {
    if (this.international) {
      this.orders.add(new InternationalOrder(this.cart));
    } else {
      this.orders.add(new LocalOrder(this.cart));
    }
    this.cart.clear();
  }

}
