package com.psybergate.grad2021.core.oopart2.hw5a;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Order {
  protected List<OrderItem> orderItems;
  protected LocalDate orderDate;
  protected double costs;

  Order(List<OrderItem> orderItems) {
    this(orderItems, 0);
  }

  Order(List<OrderItem> orderItems, double discount){
    this.orderItems = new ArrayList<>(orderItems);
    this.orderDate = LocalDate.now();
    this.costs = calcCosts(orderItems, discount);
  }

  public double getCosts() {
    return costs;
  }

  public List<OrderItem> getOrderItems() {
    return orderItems;
  }

  public static double calcCosts(List<OrderItem> orderItems, double discount){
    double sum = 0;
    for (OrderItem orderItem : orderItems){
      sum += orderItem.getTotalCosts();
    }
    return sum*(1-discount);
  }
}

class LocalOrder extends Order {

  LocalOrder(List<OrderItem> orderItems){
    this(orderItems, 0);
  }

  LocalOrder(List<OrderItem> orderItems, double discount) {
    super(orderItems, discount);
  }
}

class InternationalOrder extends Order {
  protected static double IMPORTDUTY = (-1)*0.15; // Just going with a standard 15%

  InternationalOrder(List<OrderItem> orderItems) {
    super(orderItems, IMPORTDUTY);
  }
}
