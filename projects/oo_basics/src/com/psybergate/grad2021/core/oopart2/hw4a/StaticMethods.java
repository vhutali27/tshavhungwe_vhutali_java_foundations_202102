package com.psybergate.grad2021.core.oopart2.hw4a;

public class StaticMethods {
  StaticMethods(){

  }

  public static void printHello(){
    System.out.println("Hello");
  }

  public static void polymorphicCall(StaticMethods staticMethods){
    staticMethods.printHello();
  }
  public static void main(String[] args) {
    InheritedStatic inheritedStatic = new InheritedStatic();
    inheritedStatic.printHello(); // Therefore inherited
  }
}

class InheritedStatic extends StaticMethods{
/*
  If a derived class defines a static method with the same signature as a static method in the base class
  the method in the derived class hides the method in the base class.
  Method overriding occurs dynamically(run time) that means which method is to be executed decided at run time according
  to object used for calling while static methods are looked up statically(compile time).
 */
//  @Override
//  public static void printHello(){
//    System.out.println("Hello World");
//  }
}
