package com.psybergate.grad2021.core.oopart2.hw5a3;

import java.time.LocalDate;
import java.util.List;

public abstract class DiscountPolicy {
  private double discount;

  public DiscountPolicy(double discount){
    this.discount = discount;
  }

  public void setDiscount(double discount) {
    this.discount = discount;
  }

  public double getDiscount() {
    return discount;
  }
//  public static double calculateDiscountedPrice(double price, double discount){
//    return price * (1 - discount);
//  }

}

// I know these should be in separate files, however, it's minimal code

class LocalDiscountPolicy extends DiscountPolicy {
  
  public LocalDiscountPolicy(int registrationYear) {
    this(determineDiscount(registrationYear));
  }
  
  public LocalDiscountPolicy(double discount){
    super(discount);
  }
  
  public static double determineDiscount(int registrationYear){
    double discount = 0;
    
    int currentYear = LocalDate.now().getYear();
    
    if (currentYear - registrationYear > 5) {
      discount = 0.125;
    } else if (currentYear - registrationYear > 2) {
      discount = 0.075;
    }
    return discount;
  }
}

class InternationalDiscount extends DiscountPolicy {

  public InternationalDiscount(double price){
    super(calcDiscount(price));
  }

  public void setDiscount(double discount){
    super.setDiscount(discount);
  }
  public static double calcDiscount(double price) {
    if (price > 1000000) {
      return 0.9;
    } else if (price > 500000) {
      return 0.95;
    }
    return 0;
  }

  public static double calculateCosts(List<com.psybergate.grad2021.core.oopart2.hw5a3.OrderItem> orderItems){
    double costs = 0;
    for (OrderItem orderItem : orderItems){
      costs += orderItem.getTotalCosts();
    }
    return costs;
  }
}
