package com.psybergate.grad2021.core.oopart2.ce1;

public class Account {
    protected final String accountNum;
    protected final String accountHolder;
    protected float balance;

    // TODO: 2021/02/17 A function that generates unique accountNumbers
    public Account(String accountNum, String accountHolder) {
        this.accountNum = accountNum;
        this.accountHolder = accountHolder;
        System.out.printf("In Account Constructor:\n\taccountNum: %s\n\taccountHolder:%s\n\n", this.accountNum, this.accountHolder);
    }

    public float getBalance() {
        return balance;
    }
}
