package com.psybergate.grad2021.core.oopart2.hw5a;

import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  List<Customer> customerList;

  OrderUtils(List<Customer> customerList) {
    this.customerList = customerList;
  }

  public void printCustomerOrders(){
    for (Customer customer : this.customerList){
      double sum = 0;
      for (Order order : customer.orders){
        sum += order.getCosts();
      }
      System.out.printf("Customer: %s\nTotal cost of orders: %f\n\n", customer.getName(), sum);
    }
  }

  public static void main(String[] args) {
    // Customers
    Customer customer1 = new Customer("Tyler May");
    Customer customer2 = new Customer("Selena Gomez");
    Customer internationalCustomer1 = new Customer("Jaime Gonzalez", true);
    Customer internationalCustomer2 = new Customer("Anna Mae", true);

    // Products
    Product phone = new Product("Generic Phone", 100, 5540.00);
    Product laptop = new Product("Generic Laptop", 150, 10000);
    Product usb = new Product("Generic USB", 1000, 99.99);
    Product moderm = new Product("Generic Modern", 50, 359.99);

    // customer1 orders
    customer1.addToCart(phone);
    customer1.addToCart(usb, 3);
    customer1.checkOut();

    customer1.addToCart(moderm);
    customer1.addToCart(usb);
    customer1.addToCart(laptop);
    customer1.checkOut();

    customer1.addToCart(laptop);
    customer1.checkOut();

    // Customer2
    customer2.addToCart("Home", phone, 2);
    customer2.checkOut();

    customer2.addToCart(moderm, 3);
    customer2.addToCart(laptop, 3);
    customer2.checkOut();

    customer2.addToCart(usb, 10);
    customer2.checkOut();

    // Internation Customer 1 order
    internationalCustomer1.addToCart(phone);
    internationalCustomer1.addToCart(usb, 3);
    internationalCustomer1.checkOut();

    internationalCustomer1.addToCart(moderm);
    internationalCustomer1.addToCart(usb);
    internationalCustomer1.addToCart(laptop);
    internationalCustomer1.checkOut();

    internationalCustomer1.addToCart(laptop);
    internationalCustomer1.checkOut();

    // InternationalinternationalCustomer2 order
    internationalCustomer2.addToCart("Home", phone, 3);
    internationalCustomer2.checkOut();

    internationalCustomer2.addToCart(moderm, 2);
    internationalCustomer2.checkOut();

    internationalCustomer2.addToCart(usb, 13);
    internationalCustomer2.checkOut();


    // List of all Customers
    List<Customer> customers = new ArrayList<>();
    customers.add(customer1);
    customers.add(customer2);
    customers.add(internationalCustomer1);
    customers.add(internationalCustomer2);

    // Order Utility
    OrderUtils orderUtils = new OrderUtils(customers);
    orderUtils.printCustomerOrders();
  }
}
