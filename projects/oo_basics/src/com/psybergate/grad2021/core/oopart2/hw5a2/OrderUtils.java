package com.psybergate.grad2021.core.oopart2.hw5a2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  List<Customer> customerList;

  OrderUtils(List<Customer> customerList) {
    this.customerList = customerList;
  }

  public void printCustomerOrders(){
    for (Customer customer : this.customerList){
      double sum = 0;
      for (Order order : customer.getOrders()){
        sum += order.getOrderCosts();
      }
      System.out.printf("Customer: %s\nTotal cost of orders: %f\n\n", customer.getName(), sum);
    }
  }

  public static void main(String[] args) {
    // Customers
    Customer customer1 = new Customer("Tyler May", LocalDate.parse("2015-09-27"));
    Customer customer2 = new Customer("Selena Gomez", LocalDate.parse("2005-04-10"));
    Customer internationalCustomer1 = new Customer("Jaime Gonzalez", true, LocalDate.parse("2009-10-20"));
    Customer internationalCustomer2 = new Customer("Anna May", true);

    // Products
    Product phone = new Product("Generic Phone", 500, 5540.00);
    Product laptop = new Product("Generic Laptop", 750, 10000);
    Product usb = new Product("Generic USB", 1000, 99.99);
    Product modem = new Product("Generic Modern", 500, 359.99);

    // customer1 orders
    customer1.addToCart(phone);
    customer1.addToCart(usb, 3);
    customer1.checkOut();

    customer1.addToCart(modem);
    customer1.addToCart(usb);
    customer1.addToCart(laptop);
    customer1.checkOut();

    customer1.addToCart(laptop);
    customer1.checkOut();

    // Customer2
    customer2.addToCart("Home", phone, 2);
    customer2.checkOut();

    customer2.addToCart(modem, 3);
    customer2.addToCart(laptop, 3);
    customer2.checkOut();

    customer2.addToCart(usb, 10);
    customer2.checkOut();

    // Internation Customer 1 order
    internationalCustomer1.addToCart(phone);
    internationalCustomer1.addToCart(usb, 3);
    internationalCustomer1.checkOut();

    internationalCustomer1.addToCart(modem);
    internationalCustomer1.addToCart(usb);
    internationalCustomer1.addToCart(laptop);
    internationalCustomer1.checkOut();

    internationalCustomer1.addToCart(laptop);
    internationalCustomer1.checkOut();

    // InternationalCustomer2 order
    internationalCustomer2.addToCart("Home", phone, 300);
    internationalCustomer2.checkOut();

    internationalCustomer2.addToCart(modem, 300);
    internationalCustomer2.checkOut();

    internationalCustomer2.addToCart(usb, 1000);
    internationalCustomer2.checkOut();


    // List of all Customers
    List<Customer> customers = new ArrayList<>();
    customers.add(customer1);
    customers.add(customer2);
    customers.add(internationalCustomer1);
    customers.add(internationalCustomer2);

    // Order Utility
    OrderUtils orderUtils = new OrderUtils(customers);
    orderUtils.printCustomerOrders();
  }
}
