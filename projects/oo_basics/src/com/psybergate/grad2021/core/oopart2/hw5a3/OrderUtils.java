package com.psybergate.grad2021.core.oopart2.hw5a3;

// TODO: 2021/03/01 Discount policy
// TODO: 2021/03/01 Add more Orders

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class OrderUtils {
  public static void printOrders(List<Customer> customers) {
    for (Customer customer : customers) {
      double totalCosts = 0;
      for (Order customerOrder : Order.getCustomerOrders(customer)) {
        totalCosts += customerOrder.getCosts();
      }
      System.out.printf("Customer: %s\n\tTotal Cost: %s\n", customer.getCustomerName(), totalCosts);
    }
  }

  public static void main(String[] args) {
    // Collection of Orders
    List<Customer> customers = new ArrayList<>();

    Customer customer1 = new Customer("Tyler May", LocalDate.parse("2015-09-27"));
    Customer customer2 = new Customer("Selena Gomez", "Local", LocalDate.parse("2005-04-10"));
    Customer internationalCustomer1 = new Customer("Jaime Gonzalez", "International", LocalDate.parse("2009-10-20"));
    Customer internationalCustomer2 = new Customer("Anna May", "International");

    customers.add(customer1);
    customers.add(customer2);
    customers.add(internationalCustomer1);
    customers.add(internationalCustomer2);

    // Products
    Product phone = new Product("Generic Phone", 5540.00);
    Product laptop = new Product("Generic Laptop", 750);
    Product usb = new Product("Generic USB", 199.00);
    Product modem = new Product("Generic Modern", 359.99);

    // Customer 1 OrderItems

    OrderItem orderItem1_1 = new OrderItem(phone, 100);
    OrderItem orderItem1_2 = new OrderItem(laptop, 20);
    OrderItem orderItem1_3 = new OrderItem(usb, 1500);
    OrderItem orderItem1_4 = new OrderItem(phone);
    OrderItem orderItem1_5 = new OrderItem(laptop, 250);
    OrderItem orderItem1_6 = new OrderItem(usb, 1200);
    OrderItem orderItem1_7 = new OrderItem(usb, 500);
    OrderItem orderItem1_8 = new OrderItem(laptop, 2);
    OrderItem orderItem1_9 = new OrderItem(modem, 15);

    Order customer1Order = new Order(customer1, orderItem1_1);
    customer1Order.addOrderItem(orderItem1_2);
    customer1Order.addOrderItem(orderItem1_3);

    Order customer1Order2 = new Order(customer1, orderItem1_4);
    customer1Order2.addOrderItem(orderItem1_5);
    customer1Order2.addOrderItem(orderItem1_6);
    customer1Order2.addOrderItem(orderItem1_7);

    Order customer1Order3 = new Order(customer1, orderItem1_8);
    customer1Order3.addOrderItem(orderItem1_9);

    // Customer 2 OrderItems
    OrderItem orderItem2_1 = new OrderItem(modem, 200);
    OrderItem orderItem2_2 = new OrderItem(laptop, 50);
    OrderItem orderItem2_3 = new OrderItem(usb, 900);
    OrderItem orderItem2_4 = new OrderItem(modem, 200);
    OrderItem orderItem2_5 = new OrderItem(laptop, 50);
    OrderItem orderItem2_6 = new OrderItem(usb, 900);
    OrderItem orderItem2_7 = new OrderItem(usb, 900);

    List<OrderItem> customer2OrderItems = new ArrayList<>();
    customer2OrderItems.add(orderItem2_1);
    customer2OrderItems.add(orderItem2_2);
    customer2OrderItems.add(orderItem2_3);
    Order customer2Order1 = new Order(customer2, customer2OrderItems,
        new LocalDiscountPolicy(customer2.getCustomerRegistrationDate().getYear()).getDiscount());

    Order customer2Order2 = new Order(customer2, orderItem2_4);
    customer2Order2.addOrderItem(orderItem2_5);
    customer2Order2.addOrderItem(orderItem2_6);

    Order customer2Order3 = new Order(customer2, orderItem2_7);

    // InternationCustomer 1 OrderItems
    OrderItem orderItemInternational1_1 = new OrderItem(modem, 200);
    OrderItem orderItemInternational1_2 = new OrderItem(laptop, 50);
    OrderItem orderItemInternational1_3 = new OrderItem(usb, 900);
    OrderItem orderItemInternational1_4 = new OrderItem(modem, 20);
    OrderItem orderItemInternational1_5 = new OrderItem(laptop, 790);
    OrderItem orderItemInternational1_6 = new OrderItem(usb, 9000);

    List<OrderItem> internationalCustomer1OrderItems = new ArrayList<>();
    internationalCustomer1OrderItems.add(orderItemInternational1_1);
    internationalCustomer1OrderItems.add(orderItemInternational1_2);
    internationalCustomer1OrderItems.add(orderItemInternational1_3);
    Order internationalCustomer1Order = new Order(internationalCustomer1, internationalCustomer1OrderItems,
        new InternationalDiscount(InternationalDiscount.calculateCosts(internationalCustomer1OrderItems)).getDiscount());

    List<OrderItem> internationalCustomer1OrderItems2 = new ArrayList<>();
    internationalCustomer1OrderItems2.add(orderItemInternational1_4);
    internationalCustomer1OrderItems2.add(orderItemInternational1_5);
    Order internationalCustomer1Order2 = new Order(internationalCustomer1, internationalCustomer1OrderItems2,
        new InternationalDiscount(InternationalDiscount.calculateCosts(internationalCustomer1OrderItems2)).getDiscount());

    Order internationalCustomer1Order3 = new Order(internationalCustomer1, orderItemInternational1_6);

    // InternationCustomer 2 OrderItems
    OrderItem orderItemInternational2_1 = new OrderItem(modem, 200);
    OrderItem orderItemInternational2_2 = new OrderItem(laptop, 100);
    OrderItem orderItemInternational2_3 = new OrderItem(usb, 190);
    OrderItem orderItemInternational2_4 = new OrderItem(phone, 900);
    OrderItem orderItemInternational2_5 = new OrderItem(modem, 500);
    OrderItem orderItemInternational2_6 = new OrderItem(laptop, 55);
    OrderItem orderItemInternational2_7 = new OrderItem(usb, 190);
    OrderItem orderItemInternational2_8 = new OrderItem(phone, 490);
    OrderItem orderItemInternational2_9 =new OrderItem(modem, 250);
    OrderItem orderItemInternational2_10 = new OrderItem(laptop, 40);
    OrderItem orderItemInternational2_11 = new OrderItem(usb, 195);
    OrderItem orderItemInternational2_12 = new OrderItem(phone, 200);

    List<OrderItem> internationalCustomer2OrderItems = new ArrayList<>();
    internationalCustomer2OrderItems.add(orderItemInternational2_1);
    internationalCustomer2OrderItems.add(orderItemInternational2_2);
    internationalCustomer2OrderItems.add(orderItemInternational2_3);
    internationalCustomer2OrderItems.add(orderItemInternational2_4);
    Order internationalCustomer2Order = new Order(internationalCustomer2, internationalCustomer2OrderItems, 0.2);

    Order internationalCustomer2Order2 = new Order(internationalCustomer2, orderItemInternational2_5);
    internationalCustomer2Order2.addOrderItem(orderItemInternational2_6);
    internationalCustomer2Order2.addOrderItem(orderItemInternational2_7);
    internationalCustomer2Order2.addOrderItem(orderItemInternational2_8);
    internationalCustomer2Order2.addOrderItem(orderItemInternational2_9);
    internationalCustomer2Order2.addOrderItem(orderItemInternational2_10);

    Order internationalCustomer2Order3 = new Order(internationalCustomer2, orderItemInternational2_11);
    internationalCustomer2Order3.addOrderItem(orderItemInternational2_12);


    // Print Customer orders
    OrderUtils.printOrders(customers);
  }
}
