package com.psybergate.grad2021.core.oopart1.hw4a;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    // TODO: 2021/02/12 You have 6 object attributes required no more than 5
    private final boolean isPerson; // type of customer: Person or Company
    private final String customerNum; // customer number
    private int age; // Alternatively you could preferably parse the date of birth
    private String name;
    private List<String> purchaseHistory = new ArrayList<>();
    private List<String> placedOrders = new ArrayList<>();

    private static int NUMCUSTOMERS = 100000;

    // for companies
    Customer(String companyName, int age){
        this(false, companyName, age);
    }

    Customer(Boolean isPerson, String customerName, int age){
        if (isPerson) {
            assert (age > 17) : "Underage, must be 18 or older";
            this.age = age;
        }
        this.customerNum = getCustomerNum();
        this.isPerson = isPerson;
        this.name = customerName;

    }

    /*
    Static method getCustomerNumber
    returns a unique customer identifier number
     */
    private static String getCustomerNum(){
        NUMCUSTOMERS++;
        return Integer.toString(NUMCUSTOMERS);
    }

    /*
    static method getNumberCustomers
    returns the total number of customer
     */
    public static int getNumberCustomers(){
        return NUMCUSTOMERS - 100000;
    }
    /*
    Instance methods
     */
    public String getCustomerDetails(){
        return String.format("Name: %s\nCustomer Number: %s", this.name, this.customerNum);
    }

    public List getPurcharseHistory(){
        return this.purchaseHistory;
    }

    public List getOrders(){
        return this.placedOrders;
    }

    private void processOrders(){
        this.purchaseHistory.addAll(this.placedOrders);
        this.placedOrders.clear();
    }

    public static void main(String[] args) {
        Customer person1 = new Customer(true, "Tyler May", 27);
        Customer business1 = new Customer("Company", 5);
        Customer business2 = new Customer(false, "Corporate", 90);

        System.out.printf("Customer Details\n\n%s\nTotal Customers: %d\n\n%s\nTotal Customers: %d\n\n%s\nTotal " +
                        "Customers: %d",
                person1.getCustomerDetails(), person1.getNumberCustomers(),
                business1.getCustomerDetails(), business1.getNumberCustomers(), business2.getCustomerDetails(),
                business2.getNumberCustomers());

    }
}
