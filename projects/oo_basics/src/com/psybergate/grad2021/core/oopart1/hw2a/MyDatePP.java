package com.psybergate.grad2021.core.oopart1.hw2a;

import java.time.LocalDateTime;

public abstract class MyDatePP {

    public abstract boolean validateDate(LocalDateTime date);

    public abstract void addDate(LocalDateTime[] days);

    /*
        Return 1 /0 /-1 if date1 is greater than / lesser than / equals to date2;
     */
    public abstract int greaterThan(LocalDateTime date1, LocalDateTime date2);
}
