package com.psybergate.grad2021.core.oopart1.hw3a;

public class Rectangle {
  // static variables
  private static String LIMITS = "required: length > width > 0; length <= 250, width <= 150, area <= 15000";
  // size
  private final float length;
  private final float width;
  private final float area;

  Rectangle(float length, float width) throws InstantiationException {
    if (valid(length, width)) {
      this.length = length;
      this.width = width;
      this.area = length * width;
    } else {
      throw new InstantiationException(String.format("Cannot create instance of Rectangle due to invalid " +
          "parameters; %s; passed: length " +
          "= " +
          "%f, " +
          "width = %f, " +
          "area = %f", LIMITS, length, width, length * width));
      // TODO: 2021/02/12 Chris recommends using runtime error
    }
  }

    /*
     Static Methods
     */

  public static boolean valid(float length, float width) {
    return (width > 0) && (length > width) && (length <= 250) && (width <= 150) && (length * width <= 15000);
  }

  public static void limits() {
    System.out.println(LIMITS);
  }

    /*
    Instance Methods
     */

  public static void main(String[] args) throws InstantiationException {
    Rectangle.limits();

    Rectangle rect1 = new Rectangle(40, 7);
    System.out.println(rect1.getDiagonal());
    System.out.println(rect1.getDimensions());

    Rectangle rect2 = new Rectangle(100, 50);

    Rectangle rect3 = new Rectangle(250, 15);

        /*
        Key difference between the rectangles is their dimensions
        The limits are the same for all objects
         */
  }

  public float[] getDimensions() {
    return new float[]{length, width, area};
  }

  public double getDiagonal() {
    return Math.pow((length * length + width * width), 0.5);
  }

}
