// TODO: 2021/02/11 I don't quite understand this exercise

package com.psybergate.grad2021.core.oopart1.hw2a;

import java.time.LocalDateTime;

public abstract class MyDateOO {

    LocalDateTime date;

    public abstract boolean validateDate();

    public abstract void addDate(MyDateOO date);

    /*
        Return 1 /0 /-1 if date1 is greater than / lesser than / equals to date2;
     */
    public abstract int greaterThan(MyDateOO date2);
}
