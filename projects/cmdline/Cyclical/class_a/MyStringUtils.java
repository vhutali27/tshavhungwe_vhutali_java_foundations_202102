package class_a;

import java.util.Scanner;

public class MyStringUtils{
	public static void reverse(String str){
		int len = str.length();
		String reverse_string = "";
		for (int i = len - 1; i >= 0; i--)
			reverse_string += str.charAt(i);
	
		System.out.println(reverse_string);
	}

	public static void main(String[] args) {
		class_b.StringReverse.call_a();
		Scanner in = new Scanner(System.in);
		String name = in.nextLine();
	}
}