/*


*/

import java.util.ArrayList;

public class Primes {
	
	static void calc(int n){
		int count = 0;
		if (n > 2)
			count++;

		if (n > 3)
			for (int i = 3; i <= n; i = i + 2){
				boolean factor = false;
				for (int j = 2; j < i; j++){

					if (i % j == 0){
						factor = true;
						break;
					}
					else if (i < 9)
						break;

				}
				if (!factor)
					// System.out.println(i);
					count++;
			}
		System.out.printf("%d\n", count);
	}


	public static void main(String []args) {
		final long start = System.currentTimeMillis();
		calc(2000000);
		System.out.println(System.currentTimeMillis() - start);
	}
}