/*
 The problem is such that each locker is affected by only students that are a factor of that locker number
 However given that most factors come in pairs, we changed and revert back to the initial state
 There we are looking for factors that are squares, as only one operation takes place
 It is therefore sufficient to only consider squares that are less than n

*/

import java.util.ArrayList;

public class Lockers {
	
	static void calc(int n){
		int count = 0;
		for (int i = 1; i <= n; i++){
			if (i*i < n)
				count++;

			if (i * i > n)
				break;
		}
		System.out.printf("%d\n", count);
	}


	public static void main(String []args) {
		calc(1000);
	}
}