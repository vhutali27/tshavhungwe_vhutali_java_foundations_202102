package hw1a;

public class MyStringUtils{
	public static String reverse(String str){
		int len = str.length();
		String reverse_string = "";
		for (int i = len - 1; i >= 0; i--)
			reverse_string += str.charAt(i);
	
		return reverse_string;
	}
}