package com.psybergate.javafnds;

import java.nio.charset.StandardCharsets;

public class Main {

    public static void main(String[] args) {
        String A = "This is a string";
        byte[] bytes = A.getBytes(StandardCharsets.UTF_8);
        for (byte b : bytes)
            System.out.printf("%d ", b);
    }
}
